import { NavLink } from 'react-router-dom';


function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
      </div>
      <div className="container">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfxzHFaeLRykk64ZndnYNtxbXDl-uLMu-79OqVqlpsvuIdZleDHoyCpSlpdoS-qzrT5Qc&usqp=CAU" className="shoes-img" />
        <NavLink className="nav-link" to="/shoes">Shoes</NavLink>
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiII7ktKpZskSCXvPcxEDpBWcPEOK9ysZY6w&usqp=CAU" className="hats-img" />
        <NavLink className="nav-link" to="/hats">Hats</NavLink>
      </div>
    </div>
  );
}

export default MainPage;
