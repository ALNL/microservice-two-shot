import React from 'react';
import { Link } from 'react-router-dom';

// function BinColumn(props) {
//   return (
//     <div className="col">
//       {props.list.map(data => {
//         const bin = data.bin;
//         console.log(bin)
//         return (
//           <div key={bin.href} className="card mb-3 shadow">
//             {/* <img src={bin.shoe.picture_url} className="card-img-top" /> */}
//             <div className="card-body">
//               <h5 className="card-title">{bin.closet_name}</h5>
//               {/* <h6 className="card-subtitle mb-2 text-muted">
//                 {bin.closet_name}
//               </h6> */}
//             </div>
//           </div>
//         );
//       })}
//     </div>
//   );
// }

class BinMainPage extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       binColumns: [[], [], []],
//     };
//   }

//   async componentDidMount() {
//     const url = 'http://localhost:8100/api/bins/';

//     try {
//       const response = await fetch(url);
//       if (response.ok) {
//         // Get the list of conferences
//         const data = await response.json();

//         // Create a list of for all the requests and
//         // add all of the requests to it
//         const requests = [];
//         for (let bin of data.bins) {
//           const detailUrl = `http://localhost:8100/api/bins/${bin.id}/`;
//           requests.push(fetch(detailUrl));

//         }
//         // Wait for all of the requests to finish
//         // simultaneously
//         const responses = await Promise.all(requests);

//         // Set up the "columns" to put the conference
//         // information into
//         // const binColumns = [[], [], []];
//         const binColumns = [];
//         console.log("binColumns:",binColumns);
//         // Loop over the conference detail responses and add
//         // each to to the proper "column" if the response is
//         // ok
//         let i = 0;
//         for (const binResponse of responses) {
//           if (binResponse.ok) {
//             const details = await binResponse.json();
//             binColumns[i].push(details);
//             i = i + 1;
//             if (i > 2) {
//               i = 0;
//             }
//           } else {
//             console.error(binResponse);
//           }
//         }
//         console.log(binColumns);

//         // Set the state to the new list of three lists of
//         // conferences
//         this.setState({binColumns: binColumns});
//       }
//     } catch (e) {
//       console.error(e);
//     }
//   }

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.png" alt="" width="600" />
          <h1 className="display-5 fw-bold">My shoes</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              Welcome back to your shoe wardrobe!
            </p>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/shoes/all" className="btn btn-primary btn-lg px-4 gap-3">Find my shoes</Link>
              <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Create a shoes</Link>
              {/* <Link to="/shoes/delete" className="btn btn-primary btn-lg px-4 gap-3">Delete a shoes</Link> */}
            </div>
          </div>
        </div>
        {/* <div className="container">
          <h2>My bins</h2>
          <div className="row">
            {this.state.binColumns.map((binList, index) => {
              return (
                <BinColumn key={index} list={binList} />
              );
            })}
          </div>
        </div> */}
      </>
    );
  }
}

export default BinMainPage;
