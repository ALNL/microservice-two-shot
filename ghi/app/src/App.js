import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import CreateShoeForm from './CreateShoeForm';
import BinMainPage from './BinMainPage';
import ShoesList from './ShoesList';
import Nav from './Nav';
import LocationMainPage from './LocationMainPage';
import CreateHatForm from './CreateHatForm';
import HatsList from './HatsList';


function App(props) {
  if (props.hats === undefined && props.shoes === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
            <Route path="shoes" element={<BinMainPage />} />
            <Route path="shoes/new" element={<CreateShoeForm />} />
            <Route path="shoes/all" element={<ShoesList shoes={props.shoes} />} />
            <Route path="hats" element={<LocationMainPage />} />
            <Route path="hats/new" element={<CreateHatForm />} />
            <Route path="hats/all" element={<HatsList hats={props.hats} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
