import React from 'react';
// import { Outlet } from 'react-router-dom';

class CreateShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          manufacturer: '',
          model_name: '',
          color: '',
          picture_url: '',
          bin: '',
          bins: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
        this.handleChangeModelName = this.handleChangeModelName.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
        this.handleChangeBin = this.handleChangeBin.bind(this);
      }

      async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          this.setState({ bins: data.bins });
        }
      }

      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.bins;

        // const selectTag = document.getElementById('bin');
        // const binId = selectTag.options[selectTag.selectedIndex].value;
        // const shoeUrl = `http://localhost:8080${binId}shoes/`;
        const shoeUrl = `http://localhost:8080/api/shoes/`;

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
          // window.location.href = 'http://localhost:8080/api/shoes/all/'
          // const newShoe = await response.json();
          // console.log(newShoe);
          this.setState({
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bin: '',
          });

        }
      }

      handleChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
      }

      handleChangeModelName(event) {
        const value = event.target.value;
        this.setState({ model_name: value });
      }

      handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
      }

      handleChangePictureUrl(event) {
        const value = event.target.value;
        this.setState({ picture_url: value });
      }

      handleChangeBin(event) {
        const value = event.target.value;
        this.setState({ bin: value });
      }

      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new shoes</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input value={this.state.manufacturer} onChange={this.handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.model_name} onChange={this.handleChangeModelName} placeholder="Model name" required type="model_name" name="model_name" id="model_nam" className="form-control" />
                    <label htmlFor="model_name">Model name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.color} onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.picture_url} onChange={this.handleChangePictureUrl} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture URL</label>
                  </div>
                  <div className="mb-3">
                    <select value={this.state.bin} onChange={this.handleChangeBin} required name="bin" id="bin" className="form-select">
                      <option value="">Choose a bin</option>
                      {this.state.bins.map(bin => {
                        return (
                          <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                        )
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
}

export default CreateShoeForm;
