import React from 'react';

function ShoesList(props) {
  const deleteShoe = async (id) => {
    fetch(`http://localhost:8080/api/shoes/${id}/`, {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(() => {
      window.location.reload();
  })
  }


  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Picture</th>
          <th>Manufacturer</th>
          <th>Model</th>
          <th>Color</th>
          <th>Bin</th>
          <th>Remove</th>
          <th>Shoe_id</th>
        </tr>
      </thead>
      <tbody>
        {props.shoes.map(shoe => {
          return (
            <tr key={shoe.id}>
              <td width="16.66%"><img src={shoe.picture_url} alt=''className="img-rounded" width = "50%" height="50%" /></td>
              <td width="16.66%">{ shoe.manufacturer }</td>
              <td width="16.66%">{ shoe.model_name }</td>
              <td width="16.66%">{ shoe.color }</td>
              <td width="16.66%">{ shoe.bin }</td>
              <td><button onClick={() => deleteShoe(shoe.id)} type="button" className="btn btn-primary">Delete</button></td>
              <td>{shoe.id}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
export default ShoesList;
