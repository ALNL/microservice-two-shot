import React from 'react';
// import { Outlet } from 'react-router-dom';

class DeleteHatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          fabric: '',
          style_name: '',
          color: '',
          location: '',
          locations: [],
        //   fabrics: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeFabric = this.handleChangeFabric.bind(this);
        this.handleChangeStyleName = this.handleChangeStyleName.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);
      }

      async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          this.setState({ locations: data.locations });
        //   this.setState({ fabrics: data.locations.fabric })
        }
      }

      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        // delete data.fabrics;

        // const selectTag = document.getElementById('location');
        // const locationId = selectTag.options[selectTag.selectedIndex].value;
        // const hatUrl = `http://localhost:8100${locationId}presentations/`;
        const hatUrl = `http://localhost:8090/api/locations/${data.location.id}/hats/`;

        const fetchConfig = {
          method: "delete",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
          const deletedHat = await response.json();
          console.log(deletedHat);
          this.setState({
            fabric: '',
            style_name: '',
            color: '',
            location: '',
          });
        }
      }

      handleChangeFabric(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
      }

      handleChangeStyleName(event) {
        const value = event.target.value;
        this.setState({ style_name: value });
      }

      handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
      }

      handleChangeLocation(event) {
        const value = event.target.value;
        this.setState({ location: value });
      }

      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Delete a hats</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input value={this.state.fabric} onChange={this.handleChangeFabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="fabric">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.style_name} onChange={this.handleChangeStyleName} placeholder="Style name" required type="style_name" name="style_name" id="style_nam" className="form-control" />
                    <label htmlFor="style_name">Style name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.color} onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="mb-3">
                    <select value={this.state.location} onChange={this.handleChangeLocation} required name="location" id="location" className="form-select">
                      <option value="">Choose a location</option>
                      {this.state.locations.map(location => {
                        return (
                          <option key={location.href} value={location.href}>{location.closet_name}</option>
                        )
                      })}
                    </select>
                  </div>
                  {/* <div className="mb-3">
                    <select value={this.state.fabric} onChange={this.handleChangeFabric} required name="fabric" id="fabric" className="form-select">
                      <option value="">Choose a fabric</option>
                      {this.state.fabrics.map(fabric => {
                        return (
                          <option key={fabric} value={fabric}>{fabric}</option>
                        )
                      })}
                    </select>
                  </div> */}
                  <button className="btn btn-primary">Delete</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
}

export default DeleteHatForm;
