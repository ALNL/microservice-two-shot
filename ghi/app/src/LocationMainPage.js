import React from 'react';
import { Link } from 'react-router-dom';

// function LocationColumn(props) {
//   return (
//     <div className="col">
//       {props.list.map(data => {
//         const location = data.location;
//         console.log(location)
//         return (
//           <div key={location.href} className="card mb-3 shadow">
//             {/* <img src={location.hat.picture_url} className="card-img-top" /> */}
//             <div className="card-body">
//               <h5 className="card-title">{location.closet_name}</h5>
//               {/* <h6 className="card-subtitle mb-2 text-muted">
//                 {location.closet_name}
//               </h6> */}
//             </div>
//           </div>
//         );
//       })}
//     </div>
//   );
// }

class LocationMainPage extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       locationColumns: [[], [], []],
//     };
//   }

//   async componentDidMount() {
//     const url = 'http://localhost:8100/api/locations/';

//     try {
//       const response = await fetch(url);
//       if (response.ok) {
//         // Get the list of conferences
//         const data = await response.json();

//         // Create a list of for all the requests and
//         // add all of the requests to it
//         const requests = [];
//         for (let location of data.locations) {
//           const detailUrl = `http://localhost:8100/api/locations/${location.id}/`;
//           requests.push(fetch(detailUrl));

//         }
//         // Wait for all of the requests to finish
//         // simultaneously
//         const responses = await Promise.all(requests);

//         // Set up the "columns" to put the conference
//         // information into
//         // const locationColumns = [[], [], []];
//         const locationColumns = [];
//         console.log("locationColumns:",locationColumns);
//         // Loop over the conference detail responses and add
//         // each to to the proper "column" if the response is
//         // ok
//         let i = 0;
//         for (const locationResponse of responses) {
//           if (locationResponse.ok) {
//             const details = await locationResponse.json();
//             locationColumns[i].push(details);
//             i = i + 1;
//             if (i > 2) {
//               i = 0;
//             }
//           } else {
//             console.error(locationResponse);
//           }
//         }
//         console.log(locationColumns);

//         // Set the state to the new list of three lists of
//         // conferences
//         this.setState({locationColumns: locationColumns});
//       }
//     } catch (e) {
//       console.error(e);
//     }
//   }

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.png" alt="" width="600" />
          <h1 className="display-5 fw-bold">My Hats</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              Welcome back to your hat wardrobe!
            </p>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/hats/all" className="btn btn-primary btn-lg px-4 gap-3">Find my hats</Link>
              <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Create a hat</Link>
              {/* <Link to="/hats/delete" className="btn btn-primary btn-lg px-4 gap-3">Delete a hat</Link> */}
            </div>
          </div>
        </div>
        {/* <div className="container">
          <h2>My locations</h2>
          <div className="row">
            {this.state.locationColumns.map((locationList, index) => {
              return (
                <LocationColumn key={index} list={locationList} />
              );
            })}
          </div>
        </div> */}
      </>
    );
  }
}

export default LocationMainPage;
