import React from 'react';
// import { Alert } from 'react-native';
// import { Outlet } from 'react-router-dom';

class CreateHatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          fabric: '',
          style_name: '',
          color: '',
          picture_url: '',
          location: '',
          locations: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeFabric = this.handleChangeFabric.bind(this);
        this.handleChangeStyleName = this.handleChangeStyleName.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);
      }

      async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          this.setState({ locations: data.locations });
        }
      }

      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;

        // const selectTag = document.getElementById('location');
        // const locationId = selectTag.options[selectTag.selectedIndex].value;
        // const hatUrl = `http://localhost:8090${locationId}hats/`;
        const hatUrl = `http://localhost:8090/api/hats/`;

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
          // window.location.href = 'http://localhost:8090/api/hats/all/'
          // const newHat = await response.json();
          // console.log(newHat);  
          this.setState({
            fabric: '',
            style_name: '',
            color: '',
            picture_url: '',
            location: '',
          });
        }
      }

      handleChangeFabric(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
      }

      handleChangeStyleName(event) {
        const value = event.target.value;
        this.setState({ style_name: value });
      }

      handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
      }

      handleChangePictureUrl(event) {
        const value = event.target.value;
        this.setState({ picture_url: value });
      }

      handleChangeLocation(event) {
        const value = event.target.value;
        this.setState({ location: value });
      }

      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input value={this.state.fabric} onChange={this.handleChangeFabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="fabric">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.style_name} onChange={this.handleChangeStyleName} placeholder="Style name" required type="style_name" name="style_name" id="style_nam" className="form-control" />
                    <label htmlFor="style_name">Style name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.color} onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.picture_url} onChange={this.handleChangePictureUrl} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture URL</label>
                  </div>
                  <div className="mb-3">
                    <select value={this.state.location} onChange={this.handleChangeLocation} required name="location" id="location" className="form-select">
                      <option value="">Choose a location</option>
                      {this.state.locations.map(location => {
                        return (
                          <option key={location.href} value={location.href}>{location.closet_name}</option>
                        )
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
}

export default CreateHatForm;
