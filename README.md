# Wardrobify

Team:

* Ailin Li - Shoes
* Gabriel Cruz - Hats

## Design
![](images/Design.png)

    1. Git clone the project into local repository
    2. cd into the cloned repo
    3. Run docker compose build
    4. Create a volume and name it pgdata
    5. Run the images
    6. Open browser to localhost:3000 to ensure it's working
    7. Create bins and locations within Insomnia for later use

    8. In order to make a bin/location

        - Create a post request with the attached URL to http://localhost:8100/api/bins/ or http://localhost:8100/api/locations/

        - Change the body to json and  pass in data like this

        {"closet_name": "Rainbow", "bin_number": 5, "bin_size": 20}

    After the creation of your locations/bins, you can now create shoes and hats



    After a creating a couple locations and bins to use, you can now create shoes and hats



## Shoes microservice

Construction steps:
    1. In settings.py of the Shoes Django project: Install the shoes_rest application into INSTALLED_APPS Add "wardrobe-api" and "localhost" to ALLOWED_HOSTS
    2. In shoes_rest/models, create a Shoe model with the following properties and field types: manufacturer: Charfield, model name: Charfield, color: Charfield, picture_url: URLField, bin: Foreignkey to BinVO from the wardrobe monolith with the related name "shoes" and cascades on delete
    3. In shoes_rest/models, create a BinVO model with the following properties and field types: import_href: CharField, closet_name: CharField
    4. Register the models in shoes_rest/admin.py
    5. Start the servers using the following docker commands docker compose build docker volume create pgdata docker compose up
    6. When the servers are running, make migrations for the added models in the CLI of docker for the shoes api container
    7. Set up polling so the wardrobe microservice sends a list of Bins. The Shoe model can access BinVO through a foreign key
    8. Create API Views so the user can do the following: Get a list of shoes, Create a shoes, Delete a shoes
    9. Create REACT components to display a new shoe Form and list of shoes
    10. Create a button on the ShoesList component so the user is able to delete a shoes
    11. Update the existing nav links to link to the new components

Operation steps:
    1. Open browser at http://localhost:3000/ to see the main page
    2. On main page, click the "Shoes" either on Navigation bar or on the page to go to the Shoes main page
    3. On shoes main page, there are two buttons. One is "Find my shoes", navigating to a list of my shoes. The other one is "create a new shoes", navigating to a create form.
        a). After clicking "Find my shoes", you are on the shoes list page. On this page, you can see the detailed information of all the shoes. Also, there is a "Delete" button on each shoes that can delete the shoes from the list.
        b). After clicking "Create a new shoes", you are on the create shoes form page. Fill all the information and click create. Go back to the shoes list form. Refresh !!! it and see the new shoes list.

URLs:
    1. Main Page: http://localhost:3000/
    2. Shoes main page: http://localhost:3000/shoes
    3. Show shoes list page: http://localhost:3000/shoes/all
    4. Delete shoes on list page: http://localhost:3000/shoes/all
    5. Create shoes page: http://localhost:3000/shoes/new

Ports:
    1. Wardrobe-api: 8100:8000
    2. shoes-api: 8080:8000
    3. react: 3000:3000
    4. database: 15432:5432

CRUD:
    1. GET shoes list: http://localhost:8080/api/shoes/
            Expected response data:
                Success: a list of shoes
    2. GET a certain shoe: `http://localhost:8080/api/shoes/${shoe.id}/`
            Expected response data:
                Success: the detailed information of the shoe
                Failure: {"message":"invalid shoe id"}
    3. POST new shoes on shoes list: http://localhost:8080/api/shoes/
            Sample post data:
                {
                    "manufacturer": "YSL",
                    "model_name": "Tribute",
                    "picture_url": "https://saint-laurent.dam.kering.com/m/144c3ca02097738c/Small-457752B8I009935_A.jpg?v=3",
                    "bin": "/api/bins/1/"
                }
            Expected response data:
                Success: The detail information of the added shoes;
                Failure: {"message":"invalid bin id"}
    4. DELETE a shoes on shoes list: `http://localhost:8080/api/shoes/${shoe.id}/`
            Expected response data:
                Success: {"deleted": true}
                Failure: {"message":"invalid shoe id"}

Value Object:
    1. BinVO: used as new model in the new polling Bounded Context. Poll Bin data from monolith wardrobe-api to the BinVO.
    2. Manufacturer, model name, color and picture url are also value objects of the shoe entity, to describe the shoe details.
    3. Each shoe entity is the aggregate of the aggregate root shoes list

## Hats microservice

    1. In settings.py of the Hats Django project: Install the hats_rest application into INSTALLED_APPS Add "wardrobe-api" and "localhost" to ALLOWED_HOSTS

    2. In hats_rest/models, create a Hat model with the following properties and field types: fabric: Charfield, stlye: Charfield color: Charfield, picture_url: URLField location: Foreignkey to LocationVO from the wardrobe monolith with the related name "hats" and cascades on delete

    3. In hats_rest/models, create a LocationVO model with the following properties and field types: import_href: CharField, closet_name: CharField, location_number: IntegerField, location_size: PositiveSmallIntegerField

    4. Register the models in hat_rest/admin.py

    5. Start the servers using the following docker commands docker compose build docker volume create pgdata docker compose up

    6. When the servers are running, make migrations for the added models in the CLI of docker for the hats api container

    7. Set up polling so the wardrobe microservice sends a list of Locations every minute or so. The Hat model can access LocationVO through a foreign key

    8. Create API Views so the user can do the following: Get a list of hats Create a hat Delete a hat

    9. Create REACT components to display a new Hat Form and list hats

    10. Create a button on the HatList component so the user is able to delete a hat

    11. Update the existing nav links to link to the new components

CRUD:
    1. GET hats list: http://localhost:8080/api/hats/
            Expected response data:
                Success: a list of hats
    2. GET a certain hat: `http://localhost:8090/api/hats/${hat.id}/`
            Expected response data:
                Success: the detailed information of the hat
                Failure: {"message":"invalid hat id"}
    3. POST new hats on hats list: http://localhost:8080/api/hats/
            Sample post data:
                {
                    "fabric": "Cotton",
                    "style_name": "Bucket Hat",
                    "color": "Red",
                    "picture_url": "https://media.loropiana.com/HYBRIS/FAM/FAM6527/65F795E7-55F1-4FF3-B2C7-FFC3384C36A0/FAM6527_Q050_MEDIUM.jpg",
                    "location": "/api/locations/1/"
                }
            Expected response data:
                Success: The detail information of the added hats;
                Failure: {"message":"invalid location id"}
    4. DELETE a hats on hats list: `http://localhost:8090/api/hats/${hat.id}/`
            Expected response data:
                Success: {"deleted": true}
                Failure: {"message":"invalid hat id"}
